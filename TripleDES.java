
import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;


public class TripleDES {

    private Cipher ecipher;
    private Cipher dcipher;

    public TripleDES(String key) {
        // MD5 Hash the key 
        SecretKeySpec secretKey = new SecretKeySpec(getMD5(key), "DESede");
        this.setup(secretKey);
    }

    private void setup(SecretKey key) {
        try {
            ecipher = Cipher.getInstance("DESede");
            dcipher = Cipher.getInstance("DESede");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String encrypt(String str) {
        try {
            //Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            //Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            //Encode bytes to base64 to get a string
            return this.asHex(enc);
        } catch (Exception e) {
            e.printStackTrace();
        } 
        return null;
    }

    public String decrypt(String str) {
        try {
            String originalString = new String(dcipher.doFinal(this.hexToByte(str)), "UTF-8");

            return originalString;
        } catch (Exception e) {
            e.printStackTrace();
        } 
        return null;
    }

    private String asHex(byte buffer[]) {
        StringBuffer strBuf = new StringBuffer(buffer.length * 2);
        int i;

        for (i = 0; i < buffer.length; i++) {
            if (((int) buffer[i] & 0xff) < 0x10) {
                strBuf.append("0");
            }
            strBuf.append(Long.toString((int) buffer[i] & 0xff, 16));
        }
        return strBuf.toString();
    }

    private byte[] hexToByte(String hexString) {
        int len = hexString.length();
        byte[] ba = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            ba[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i + 1), 16));
        }
        return ba;
    }


    private static byte[] getMD5(String input) {
        try {
            byte[] bytesOfMessage = input.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] keyBytes = Arrays.copyOf(bytesOfMessage, 24);

            for (int j = 0, k = 16; j < 8;) {
                keyBytes[k++] = keyBytes[j++];
            }

            return keyBytes;
        } catch (Exception e) {
            return null;
        }
    }
}
