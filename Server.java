
import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.AlgorithmParameterGenerator;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.X509EncodedKeySpec;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.xml.bind.DatatypeConverter;


// The server
public class Server {

    // Server properties, including thread safe handling of up to two connections
    private int serverPort = 15000;
    private ServerSocket serverSocket;
    private Hashtable<String, ConnectionService> connections = new 
        Hashtable<String, ConnectionService>();

    public Server() {
        // Boot up the server 
        try {
            serverSocket = new ServerSocket(serverPort);
            System.out.println("Waiting...");
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Connected: " + socket);
                ConnectionService service = new ConnectionService(socket);
                service.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new Server();
    }

    // Manages the communication between the client and the server
    class ConnectionService extends Thread {

        private Socket socket;

        private AlgorithmParameterGenerator algorithmParamGen;
        private DHParameterSpec parameterSpec;
        private KeyPairGenerator keyPairGen;
        private KeyPair keyPair;

        private BigInteger primeModulus;
        private BigInteger baseGenerator;
        private String publicKey;

        private BufferedReader inputReader;
        private PrintWriter outputWriter;
        private String username;

        private TripleDES tripleDES;

        public ConnectionService(Socket socket) {
            this.socket = socket;

            try {
                // Generate the properties for the Deffie Hellman key exchange
                this.algorithmParamGen = AlgorithmParameterGenerator.
                    getInstance("DH");
                this.algorithmParamGen.init(512);
                this.parameterSpec = algorithmParamGen.generateParameters().
                    getParameterSpec(DHParameterSpec.class);

                this.primeModulus = parameterSpec.getP();
                this.baseGenerator = parameterSpec.getG();

                this.keyPairGen = KeyPairGenerator.getInstance("DH");
                this.keyPairGen.initialize(parameterSpec);
                this.keyPair = this.keyPairGen.generateKeyPair();

                this.publicKey = byteToHex(this.keyPair.getPublic().getEncoded());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                inputReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                outputWriter = new PrintWriter(socket.getOutputStream(), true);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        private String byteToHex(byte[] array) {
            return DatatypeConverter.printHexBinary(array);
        }

        private byte[] hexToByte(String string) {
            return DatatypeConverter.parseHexBinary(string);
        }

        @Override
        public void run() {
            try {
                while (true) {
                    String receivedMessage = inputReader.readLine();

                    // 
                    if (receivedMessage.contains("Signin ")) {
                        if (connections.size() >= 2) {
                            sendMessage("Error: Server is full, please wait until a client leaves.");
                        } else {
                            username = receivedMessage.split("Signin ")[1];
                            if (connections.containsKey(username)) {
                                sendMessage("Error: username taken apply with a different username");                                                  //Βγαινει και συνεχίζει το loop?
                            } else {
                                sendToAnyone(username + " signed in");
                                connections.put(username, this);
                                sendToSomeOne(username, "P:" + this.primeModulus);
                                sendToSomeOne(username, "G:" + this.baseGenerator);
                                sendToSomeOne(username, "Key:" + this.publicKey);
                            }
                        }
                    } else if (receivedMessage.contains("Key:")) {
                        String clientPublicKey = receivedMessage.split("Key:")[1];
                        try {
                            DHPublicKey servCliPub = (DHPublicKey) KeyFactory.
                                getInstance("DH").generatePublic(
                                    new X509EncodedKeySpec(hexToByte(clientPublicKey)));
                            
                            KeyAgreement serverKeyAgreement = KeyAgreement.getInstance("DH");
                            serverKeyAgreement.init(keyPair.getPrivate(), parameterSpec);
                            serverKeyAgreement.doPhase(servCliPub, true);
                            byte[] serverSecretKey = serverKeyAgreement.generateSecret();

                            // Initialise the TripleDES encryption method using the established secret
                            // key
                            this.tripleDES = new TripleDES(new String(serverSecretKey, "UTF-8"));
                        } catch (Exception e) {

                        }
                    } else {
                        // Receive the message, then decrypt it and sent it back
                        System.out.println("Received message: " + receivedMessage);
                        System.out.println("Decrypted message: " + this.tripleDES.decrypt(receivedMessage));
                        sendToAnyone(username + ": " + this.tripleDES.decrypt(receivedMessage));
                    }
                }

            } catch (IOException e) {
                System.out.println(e.getMessage());
            } catch (NullPointerException e) {
                System.out.println(e.getMessage());
            } finally {
                outputWriter.close();
            }

        }

        private void sendMessage(String message) {
            outputWriter.println(message);
        }

        // Send to a specific Client
        private void sendToSomeOne(String username, String message) {
            ConnectionService connection = connections.get(username);
            connection.sendMessage(message);
        }

        // Broadcast to all clients
        private void sendToAnyone(String message) {
            Iterator iterator = connections.keySet().iterator();
            while (iterator.hasNext()) {
                ConnectionService connection = connections.get(iterator.next());
                connection.sendMessage(message);
            }
        }
    }
}
