
Secure Network Programming
--------------------------

By s3282453

Assignment #1
=============

###Instructions###
To run the server and two clients, use the commands.

javac Server.java
java Server

javac Client.java
java Client
java Client

###Info###
* Uses Deffie Hellman for the symmetric key exchange
* Uses Triple DES for the encryption
