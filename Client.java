
import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.AlgorithmParameterGenerator;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.X509EncodedKeySpec;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Scanner;
import javax.xml.bind.DatatypeConverter;


public class Client
{
    // Client properties relating to the connection and encryption
    private static Socket socket;
    private static int port = 15000;
    private static String IPAddress = "127.0.0.1";
    private static String username;

    private static String primeModulus;
    private static String baseGenerator;
    private static String serverPublicKey;
    private static KeyPairGenerator keyPairGen;

    private static BufferedReader inputReader;
    private static PrintWriter outputWriter;

    private static TripleDES tripleDES;

    private static byte[] clientSecret;

    public static void main(String[] args) throws Exception 
    { 
        Scanner input = new Scanner(System.in);
        BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));

        // Prompt for a username and then try to connect to the server
        System.out.println("Welcome Client... Please choose your username.");
        System.out.print("Username: ");
        username = input.nextLine();

        connect(username);

        // Initialise the TripleDES encryption method using the established secret
        // key
        tripleDES = new TripleDES(new String(clientSecret, "UTF-8"));

        String receiveMessage, sendMessage;
        while (true)
        {
            try {
                System.out.print("Send Message: ");
                sendMessage = keyRead.readLine();
                sendData(tripleDES.encrypt(sendMessage));  

                if((receiveMessage = inputReader.readLine()) != null)
                {
                    System.out.println(receiveMessage);
                } } catch (Exception e){
                    e.printStackTrace();
                }
        }               
    }

    // Establish a connection to the server
    private static void connect(String username) {
        boolean flag = false;
        String receiveMessage;

        try {
            socket = new Socket(InetAddress.getByName(IPAddress), port);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Connection failure...");
            System.exit(0);
        }
        try {
            // Signin with the username
            inputReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outputWriter = new PrintWriter(socket.getOutputStream(), true);
            
            sendData("Signin " + username);

            // Continue until the keys have been exchanged.
            while (flag == false) {
                if((receiveMessage = inputReader.readLine()) != null)
                    flag = keyExchange(receiveMessage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sendData(String messageToSend) {
        System.out.println(messageToSend);
        outputWriter.println(messageToSend);
    }

    // Exchange all the information needed to generate a secret key:
    // Prime, Base and Public Keys
    private static boolean keyExchange(String message) {
        if (message.contains("P:"))
            primeModulus = message.split("P:")[1];
        else if (message.contains("G:"))
            baseGenerator = message.split("G:")[1];
        else if (message.contains("Key:"))
            serverPublicKey = message.split("Key:")[1];
        else if (message.contains("Error:")) {
            System.out.println(message);
            System.exit(0);
        }

        if (primeModulus != null && baseGenerator != null 
            && serverPublicKey != null) {
            try {
                DHParameterSpec parameterSpec = new DHParameterSpec(
                    new BigInteger(primeModulus), new BigInteger(baseGenerator));

                keyPairGen = KeyPairGenerator.getInstance("DH");
                keyPairGen.initialize(parameterSpec);
                KeyPair keyPair = keyPairGen.generateKeyPair();

                String clientPublicKey = byteToHex(keyPair.getPublic().getEncoded());

                // Send the server back the client's public key so that a private
                // key can be generated.
                sendData("Key:" + clientPublicKey);

                // Generate the secret key
                DHPublicKey clientServerPublicKey = (DHPublicKey) KeyFactory.getInstance("DH")
                    .generatePublic(new X509EncodedKeySpec(hexToByte(serverPublicKey)));
                KeyAgreement clientKeyAgreement = KeyAgreement.getInstance("DH");
                clientKeyAgreement.init(keyPair.getPrivate(), parameterSpec);
                clientKeyAgreement.doPhase(clientServerPublicKey, true);
                clientSecret = clientKeyAgreement.generateSecret();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // We have generated the key, return.
            return true;
        }
        // We don't have the key yet.
        return false;
    }

    private static String byteToHex(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    private static byte[] hexToByte(String string) {
        return DatatypeConverter.parseHexBinary(string);
    }                 
}
